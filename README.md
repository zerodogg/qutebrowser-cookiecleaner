# qutebrowser-cookiecleaner

This is a stop-gap hack to add support for cookie whitelists to the
[qutebrowser](http://www.qutebrowser.org/), until the browser actually supports
it by itself.

It takes a single file with a list of domains that are permitted to have
cookies (and IndexedDB, databases and Local Storage). It deletes everything
else. It can be used in `--wrap` mode that wraps qutebrowser and performs the
cleanup when qutebrowser starts and exits.

## Installation

Clone the git repo, run `make install`.

## Configuration

Edit `XDG_CONFIG_HOME/qutebrowser-cookiecleaner.list` (usually
`~/.config/qutebrowser-cookiecleaner.list`). Each line is a single
domain. That domain and **all** subdomains will be whitelisted. Any line
starting with `#` (optionally prefixed by whitespace) is ignored:

Example:

    lwn.net
    gitlab.org

## Usage

Either run `qutebrowser-cookiecleaner` by itself to perform a one-off deletion.
Or run `qutebrowser-cookiecleaner --wrap qutebrowser` to start `qutebrowser`
and perform the cleanup once before starting qutebrowser, and once after it
exits.

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/gpl-3.0.txt.
