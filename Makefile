# qutebrowser-cookiecleaner makefile

VERSION=$(shell ./qutebrowser-cookiecleaner --version|perl -p -e 's/^\D+//; chomp')

ifndef prefix
# This little trick ensures that make install will succeed both for a local
# user and for root. It will also succeed for distro installs as long as
# prefix is set by the builder.
prefix=$(shell perl -e 'if($$< == 0 or $$> == 0) { print "/usr" } else { print "$$ENV{HOME}/.local"}')

# Some additional magic here, what it does is set BINDIR to ~/bin IF we're not
# root AND ~/bin exists, if either of these checks fail, then it falls back to
# the standard $(prefix)/bin. This is also inside ifndef prefix, so if a
# prefix is supplied (for instance meaning this is a packaging), we won't run
# this at all
BINDIR ?= $(shell perl -e 'if(($$< > 0 && $$> > 0) and -e "$$ENV{HOME}/bin") { print "$$ENV{HOME}/bin";exit; } else { print "$(prefix)/bin"}')
endif

BINDIR ?= $(prefix)/bin
DATADIR ?= $(prefix)/share

CORE_DISTFILES = COPYING Makefile README.md
DISTFILES = $(CORE_DISTFILES) qutebrowser-cookiecleaner

# Install qutebrowser-cookiecleaner
install:
	mkdir -p "$(BINDIR)"
	cp qutebrowser-cookiecleaner "$(BINDIR)"
	chmod 755 "$(BINDIR)/qutebrowser-cookiecleaner"
localinstall:
	mkdir -p "$(BINDIR)"
	ln -sf $(shell pwd)/qutebrowser-cookiecleaner $(BINDIR)/
# Uninstall an installed qutebrowser-cookiecleaner
uninstall:
	rm -f "$(BINDIR)/qutebrowser-cookiecleaner"
	rm -rf "$(DATADIR)/qutebrowser-cookiecleaner"
# Clean up the tree
clean:
	rm -f `find|egrep '~$$'`
	rm -f qutebrowser-cookiecleaner-*.tar.bz2 qutebrowser-cookiecleaner-*.tar.bz2.sig
	rm -rf qutebrowser-cookiecleaner-$(VERSION)
# Verify syntax
sanity:
	@perl -c qutebrowser-cookiecleaner
# Create the tarball
distrib: clean
	mkdir -p qutebrowser-cookiecleaner-$(VERSION)
	cp -r $(DISTFILES) ./qutebrowser-cookiecleaner-$(VERSION)
	tar -jcvf qutebrowser-cookiecleaner-$(VERSION).tar.bz2 ./qutebrowser-cookiecleaner-$(VERSION)
	rm -rf qutebrowser-cookiecleaner-$(VERSION)
	gpg --sign --detach-sign qutebrowser-cookiecleaner-$(VERSION).tar.bz2
